package com.umg.geometria;

import com.umg.poo.model.Circulo;
import com.umg.poo.model.FiguraGeometrica;
import com.umg.poo.model.Rectangulo;
import com.umg.poo.model.Triangulo;

import java.util.ArrayList;
import java.util.List;

public class Mantenimiento {

    public static void main(String[] args) {

        // operador diamante se tipifica el el dato
        List<Circulo> listaCirculo = new ArrayList<Circulo>();
        List<Triangulo> listaTriangulo = new ArrayList<Triangulo>();
        List<Rectangulo> listaRectangulo = new ArrayList<Rectangulo>();

        List<FiguraGeometrica> listaGenerica = new ArrayList<FiguraGeometrica>();

        FiguraGeometrica objGenerico01 = new Circulo();
        FiguraGeometrica objGenerico02 = new Rectangulo();

        Circulo objCirculo01 = (Circulo) objGenerico01;
        Rectangulo objRectangulo01 = (Rectangulo) objGenerico02;


        listaGenerica.add(new Circulo());
        listaGenerica.add(new Rectangulo());
        listaGenerica.add(new Triangulo());

        listaGenerica.get(0).getArea();
        listaGenerica.get(1).getArea();
        listaGenerica.get(2).getArea();



        listaCirculo.add(new Circulo());
        listaTriangulo.add(new Triangulo());
        listaRectangulo.add(new Rectangulo());



    }

}
