package com.umg.poo.model;

public class Circulo extends FiguraGeometrica {
    private int radio;

    public double getArea() {
        return 10;
    }

    public int getDiametro() {
        return radio * 2;
    }

    public double getCircunferia() {
        return 2 * 3.1416 * radio;
    }
}
