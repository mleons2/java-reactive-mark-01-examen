package com.umg.poo.model;

public class Rectangulo extends FiguraGeometrica {
    private int base;
    private int altura;

    public double getArea() {
        return base * altura;
    }

    public int getPerimetro() {
        return 2 * altura + 2 * base;
    }

}
