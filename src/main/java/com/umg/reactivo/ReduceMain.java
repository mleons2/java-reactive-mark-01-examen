/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.umg.reactivo;

import rx.Observable;
import rx.functions.Func2;

/**
 * @author gluna
 */
public class ReduceMain {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        Integer[] numbers = {1, 2, 3, 4, 5};


        Integer resultado;

        Observable miobservable = Observable.from(numbers).reduce(
                new Func2<Integer, Integer, Integer>() {
                    @Override
                    public Integer call(Integer sum, Integer actual) {

                        return sum + actual;
                    }
                }
        );
        miobservable.subscribe((sumatoria) -> {
            System.out.println("resultado1:" + sumatoria);
            // enviar a bd
            // enviar a formulario
        });
        miobservable.subscribe((sumatoria) -> {
            System.out.println("resultado2:" + sumatoria);

            // enviar a servicio rest
        });


    }

}
