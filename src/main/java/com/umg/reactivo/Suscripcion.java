package com.umg.reactivo;

import rx.Observable;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * @author gluna
 */
public class Suscripcion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        String[] periodicos = {"demoa", "luis", "ajuan", "pedro"};

        Observable prensalibre = Observable.from(periodicos);

        Action1 leerPeriodico = new Action1<String>() {

            @Override
            public void call(String s) {

                System.out.println("Hello " + s + "!");
            }

        };

        prensalibre.subscribe(leerPeriodico);

    }
    //filtro
      /*  Observable.from(names).filter(

                new Func1<String, Boolean>() {
                    @Override
                    public Boolean call(String t) {

                        return t.contains("a");
                    }

                }// ordenamiento
        ).sorted().subscribe(
                new Action1<String>() {

                    @Override
                    public void call(String s) {

                        System.out.println("Hello " + s + "!");
                    }

                });

    } */

}
